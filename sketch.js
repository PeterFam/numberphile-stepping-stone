let canvasSize = window.innerWidth; // Canvas Size
let size = 20; // Hexagon Size
let prototyping = true; // Every 2 Black Pieces are valid
let counter = 2; // The Starting Cointer
let mousePressed = false; // Debouncing Variable
let c = 0; // Hexagon Color
let positionArray = {}; // Object to hold filled Positions
let pieceInHand = false;
let playerScores = [0, 0];
let playerColors = [0, 0];
let playerTurn = 0;
let updateTurn = () => {
  playerTurn = (playerTurn + 1) % 2;
};
let updateScore = (value) => {
  playerScores[playerTurn] += value;
  document.getElementById("player1-score").innerHTML = playerScores[0];
  document.getElementById("player2-score").innerHTML = playerScores[1];
  updateTurn();
  document.getElementById("turn").innerHTML = `Player ${playerTurn + 1} Turn`;
};
/**
 * Get Hexagon Value on the grid
 */
let getHexaValue = (q, r) => {
  return positionArray[q] ? (positionArray[q][r] ? positionArray[q][r] : 0) : 0;
};
/**
 * Set Hexagon Value on the grid
 */
let setHexaValue = (q, r, value) => {
  if (positionArray[q] === undefined) {
    positionArray[q] = {};
  }
  positionArray[q][r] = value;
};
let neighbors = [
  [1, -1],
  [1, 0],
  [0, -1],
  [0, 1],
  [-1, 0],
  [-1, 1],
]; // Neighbors Indexes
/**
 * Draw Hexagon Centered at x, y with specific size
 */
let Hexagon = (x, y, size, pressed, brown, value) => {
  fill(c);
  stroke(200);
  let h = size * 2;
  let w = size * sqrt(3);
  beginShape();
  vertex(x + 0, y + 0.5 * h);
  vertex(x + 0.5 * w, y + 0.25 * h);
  vertex(x + 0.5 * w, y + -0.25 * h);
  vertex(x + 0, y + -0.5 * h);
  vertex(x + -0.5 * w, y + -0.25 * h);
  vertex(x + -0.5 * w, y + 0.25 * h);
  endShape(CLOSE);
  if (pressed === true) {
    if (brown === true) {
      fill(0);
      ellipse(x, y, w * 0.75, w * 0.75);
    } else {
      fill(playerColors[playerTurn]);
      ellipse(x, y, w * 0.75, w * 0.75);
      fill(0);
      hexaText(value, x, y, false);
    }
    fill(c);
  }
};
/*
 * Draws Text (number) centerd at X, Y
 */
let hexaText = (value, x, y, light) => {
  if (light) {
    fill(200);
    noStroke();
  }
  text(
    value + "",
    x - size * 0.25 - (value >= 10 ? 0.25 * size : 0),
    y + size * 0.25
  );
  fill(c);
  stroke(200);
};
/**
 * get  coord (q, r) in (x, y)
 */
let getHexaCoordXY = (q, r) => {
  let x = round(canvasSize / 2) + size * (sqrt(3) * q + (sqrt(3) / 2) * r);
  let y = round(canvasSize / 2) + size * ((3 / 2) * r);
  return { x, y };
};
/**
 * get coord (x,y) in (q,r)
 */
let getHexaCoordQR = (x, y) => {
  let q =
    ((sqrt(3) / 3) * (x - round(canvasSize / 2)) -
      (1.0 / 3) * (y - round(canvasSize / 2))) /
    size;
  let r = ((2.0 / 3) * (y - round(canvasSize / 2))) / size;
  return { q: round(q), r: round(r) };
};
/*
 * Draws Hexagon at Coord (q,r)
 */
let Hexa = (q, r, pressed, brown, value) => {
  let { x, y } = getHexaCoordXY(q, r);
  Hexagon(x, y, size, pressed, brown, value);
};
/*
 * Get Neighbors Hexagons' value Sum at (q,r)
 */
let getHexaSum = (q, r) => {
  let values = neighbors.map((el) => {
    return getHexaValue(el[0] + q, el[1] + r);
  });
  const reducer = (accumulator, curr) => accumulator + curr;
  return values.reduce(reducer, 0);
};
/*
 * Check if a counted Piece can be placed at (q,r)
 */
let validPosition = (q, r) => {
  if (getHexaValue(q, r) !== 0) {
    return false;
  }
  let sum = getHexaSum(q, r);
  if (counter === sum && getHexaValue(q, r) === 0) {
    return true;
  }
  let values = neighbors.map((el) => {
    return getHexaValue(el[0] + q, el[1] + r);
  });
  if (
    sum === 2 &&
    prototyping === true &&
    values.every((el, index, arr) => el <= 1)
  ) {
    counter = 2;
    return true;
  }
  return false;
};
/*
 * Reset Game
 */
function reset() {
  positionArray = {};
  counter = 2;
  background(50);
  for (
    let j = -round(canvasSize / size / 2);
    j <= round(canvasSize / size / 2);
    j++
  ) {
    for (
      let i = -round(canvasSize / size / 2);
      i <= round(canvasSize / size / 2);
      i++
    ) {
      Hexa(i, j, size, false, false, 0);
    }
  }
  playerScores = [0, 0];
  playerTurn = 0;
  updateScore(0);
  updateScore(0);
}
// Setup function
function setup() {
  c = color(240);
  playerColors = [color(243, 230, 102), color(100, 234, 239)];
  createCanvas(canvasSize, canvasSize);
  angleMode(DEGREES);
  textSize(size * 0.75);
  reset();
}
function draw() {
  let { q, r } = getHexaCoordQR(mouseX, mouseY);
  document.getElementById("sum").innerHTML = getHexaSum(q, r);
  if (mouseIsPressed && mousePressed == false) {
    let { q, r } = getHexaCoordQR(mouseX, mouseY);
    if (validPosition(q, r) && pieceInHand === false) {
      Hexa(q, r, true, false, counter);
      setHexaValue(q, r, counter);
      counter++;
      updateScore(1);
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else {
      document.getElementById("move").innerHTML = "Invalid Move";
      document.getElementById("move").style.color = "rgb(191 18 39)";
    }
    checkPossibleSums();
    mousePressed = true;
  } else if (mouseIsPressed) {
    mousePressed = true;
  } else {
    mousePressed = false;
  }
}
/**
 * check all possibleSums around Placed Pieces
 */
let checkPossibleSums = () => {
  for (let el of neighbors) {
    for (let q of Object.keys(positionArray)) {
      qindex = parseInt(q) + el[0];
      for (let r of Object.keys(positionArray[q])) {
        r = parseInt(r) + el[1];
        let { x, y } = getHexaCoordXY(qindex, r);
        let sum = getHexaSum(qindex, r);
        if (getHexaValue(qindex, r) === 0) {
          Hexa(qindex, r, false, false);
          if (sum >= counter - 1) hexaText(sum, x, y, true);
        }
      }
    }
  }
};
function keyTyped() {
  if (key === " ") reset();
  // Place A Black Piece
  let { q, r } = getHexaCoordQR(mouseX, mouseY);
  if (key === "a") {
    if (getHexaValue(q, r) === 0) {
      Hexa(q, r, true, true, 0);
      setHexaValue(q, r, 1);
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else {
      document.getElementById("move").innerHTML = "Invalid Move";
      document.getElementById("move").style.color = "rgb(191 18 39)";
    }
  }
  if (key === "z") {
    if (getHexaValue(q, r) === counter - 1 && getHexaValue(q, r) !== 1) {
      Hexa(q, r, false, false);
      setHexaValue(q, r, 0);
      counter--;
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else {
      document.getElementById("move").innerHTML = "Invalid Move";
      document.getElementById("move").style.color = "rgb(191 18 39)";
    }
  }
  if (key === "x") {
    if (getHexaValue(q, r) === 1) {
      Hexa(q, r, false, false);
      setHexaValue(q, r, undefined);
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else {
      document.getElementById("move").innerHTML = "Invalid Move";
      document.getElementById("move").style.color = "rgb(191 18 39)";
    }
  }
  if (key === "m") {
    if (getHexaValue(q, r) === 1 && pieceInHand === false) {
      Hexa(q, r, false, false);
      pieceInHand = true;
      setHexaValue(q, r, undefined);
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else if (getHexaValue(q, r) === 0 && pieceInHand === true) {
      Hexa(q, r, true, true, 0);
      setHexaValue(q, r, 1);
      pieceInHand = false;
      updateScore(-1);
      document.getElementById("move").innerHTML = "Valid Move";
      document.getElementById("move").style.color = "rgb(13 117 37)";
    } else {
      document.getElementById("move").innerHTML = "Invalid Move";
      document.getElementById("move").style.color = "rgb(191 18 39)";
    }
  }
  checkPossibleSums();
  return false;
}
