# Numberphile Stepping Stone

This "Puzzle" is an implementation of the Stepping Stone Puzzle mentioned in [this numberphile video](https://youtu.be/m4Uth-EaTZ8)
The only change that i made is placing pieces on a hexagonal grid cause i thought it's more interesting.

It was created using [P5.JS](https://p5js.org) and plain HTML.
